const mongoose = require('mongoose');

const CarsSchema = new mongoose.Schema({
    photo: {
        type : String,
        required : true
    },
    brand: {
        type : String,
        required : true
    },
    vapemodel: {
        type: String, 
        required: true,
    },
    wattz: {
        type: Number, 
        required: true,
    },
    vapestatus: {
        type : String,
        required : false
    },
    color: {
        type: String,
        required : true
    },
    last_modified_by: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user'
    },
    date: {
        type: Date,
        default: Date.now
    }
})

module.exports = mongoose.model('vapes', CarsSchema);