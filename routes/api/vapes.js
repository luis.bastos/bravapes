const express = require('express');
const Vapes = require('../../models/vapes');
const { check, validationResult } = require('express-validator');
const router = express.Router();
const auth = require('../../middleaware/auth');
const file = require('../../middleaware/file')
const MSGS = require('../../messages')
const config = require('config');

router.get('/',async (req, res, next) => {
  try {
    let varvape = await Vapes.find(req.query).populate('company')
    const BUCKET_PUBLIC_PATH = process.env.BUCKET_PUBLIC_PATH || config.get('BUCKET_PUBLIC_PATH')
    varvape = varvape.map(function(vapes) {
      vapes.photo = `${BUCKET_PUBLIC_PATH}${vapes.photo}`
      return vapes
    })
    res.json(varvape)
  } catch (err) {
    console.error(err.message)
    res.status(500).send({ "error": MSGS.GENERIC_ERROR })
  }
})

router.get('/:userId', [], async (req, res, next) => {
  try {
    const id = req.params.userId
    const user = await Vapes.findOne({_id : id})
    if (user) {
      res.json(user)
    } else {
      res.status(404).send({ "error": "user not found" })
    }
  } catch (err) {
    console.error(err.message)
    res.status(500).send({ "error": "Server Errorr" })
  }
})

router.delete('/:userId', async(req, res, next) => {
  try {
    const id = req.params.userId
    const user = await Vapes.findOneAndDelete({_id : id})
    if (user) {
      res.json(user)
    } else {
      res.status(404).send({ "error": "user not found" })
    }
  } catch (err) {
    console.error(err.message)
    res.status(500).send({ "error": "Server Error" })
  }
})

router.patch('/:userId', auth, file, async (req, res, next) => {
  try {
      req.body.last_modified_by = req.user.id
      if(req.body.photo_name){
        req.body.photo = `vapes/${req.body.photo_name}`
      }
      const zzz = await Vapes.findByIdAndUpdate(req.params.userId, { $set: req.body }, { new: true })
      if(zzz){
          res.json(zzz)
      }else{
          res.status(404).send({ "error": MSGS.PRODUCT404 }) 
      }
  } catch (err) {
    console.error(err.message)
    res.status(500).send({ "error": MSGS.GENERIC_ERROR })
  }
})

router.post('/', [
  check('brand', 'brand is not valid').not().isEmpty(),
  check('vapemodel', 'Please inform your vapemodel').not().isEmpty(),
  check('wattz', 'Please insert a voltage').not().isEmpty(),
  check('vapestatus', 'Please inform the vapestatus').not().isEmpty(),
  check('color', 'Please insert a color').not().isEmpty(),
], auth, file, async (req, res, next) => {
  try {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() })
    } else {
      req.body.photo = `vapes/${req.body.photo_name}`
      let vapes = new Vapes(req.body)
      vapes.last_modified_by = req.user.id
      await vapes.save()
      if (vapes.id) {
        const BUCKET_PUBLIC_PATH = process.env.BUCKET_PUBLIC_PATH || config.get('BUCKET_PUBLIC_PATH')
        vapes.photo = `${BUCKET_PUBLIC_PATH}${vapes.photo}`
        res.json(vapes)
      }
    }
  } catch (err) {
    console.error(err.message)
    res.status(500).send({ "error": MSGS.GENERIC_ERROR })
  }
}) 

module.exports = router;